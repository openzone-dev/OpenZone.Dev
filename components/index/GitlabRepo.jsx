import React, {Component} from "react";
import style from "./GitlabRepo.module.css"
import IndexItem from "./IndexItem";


function GitlabProject(props) {
    return(
        <div className={style.project}>
            <a href={props.link}
               target="_blank"
               rel="noopener noreferrer"
               className={`${style.project_title} ${style.description}`}>
                    {props.title}
            </a>
            <p className={style.description}>
                {props.description}
            </p>
            <div className={style.stats}>
                <span className={`material-symbols-outlined ${style.icon}`}>
                    grade
                </span>
                <span className={`${style.description} ${style.right_spacer}`}>
                    {props.favorites}
                </span>
                <span className={`material-symbols-outlined ${style.icon}`}>
                    call_split
                </span>
                <span className={`${style.description} ${style.right_spacer}`}>
                    {props.forks}
                </span>
            </div>
        </div>
    );
}


export default class GitlabRepo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            repos: ""
        };
    }

    async componentDidMount() {
        const repos = [];

        const response = await fetch("https://gitlab.com/api/v4/groups/51639518/");
        const projects = await response.json();
        for (const project of projects["projects"]) {
            repos.push(
                <GitlabProject
                    key={project.id}
                    title={project.name}
                    description={project.description}
                    favorites={project.star_count}
                    forks={project.forks_count}
                    link={project.web_url}
                />
            );
        }

        this.setState({
            repos: repos
        });
    }

    render() {
        return (
            <IndexItem title={"Projects"}>
                {this.state.repos}
            </IndexItem>
        );
    }
}