import React, {Component} from "react";
import style from "./GitlabRepo.module.css"
import IndexItem from "./IndexItem";


function DockerHubImage(props) {
    return(
        <div className={style.project}>
            <a href={props.link}
               target="_blank"
               rel="noopener noreferrer"
               className={`${style.project_title} ${style.description}`}>
                    {props.title}
            </a>
            <p className={style.description}>
                {props.description}
            </p>
            <div className={style.stats}>
                <span className={`material-symbols-outlined ${style.icon}`}>
                    grade
                </span>
                <span className={`${style.description} ${style.right_spacer}`}>
                    {props.favorites}
                </span>
                <span className={`material-symbols-outlined ${style.icon}`}>
                    file_download
                </span>
                <span className={`${style.description} ${style.right_spacer}`}>
                    {props.downloads}
                </span>
            </div>
        </div>
    );
}


export default class DockerHub extends Component {
    constructor(props) {
        super(props);
        this.state = {
            images: ""
        };
    }

    async componentDidMount() {
        const images = [];
        let counter = 1;

        const response = await fetch(`https://api.allorigins.win/get?url=${encodeURIComponent('https://hub.docker.com/v2/repositories/openzonedev/?page_size=100')}`);
        const responseDecoded = JSON.parse((await response.json())["contents"]);
        for (const image of responseDecoded["results"]) {
            const imageSpecificsResponse = await fetch(`https://api.allorigins.win/get?url=${encodeURIComponent(`https://hub.docker.com/v2/repositories/openzonedev/${image.name}/`)}`)
            const imageSpecifics = JSON.parse((await imageSpecificsResponse.json())["contents"]);

            images.push(
                <DockerHubImage
                    key={counter++}
                    title={`${imageSpecifics.namespace}/${imageSpecifics.name}`}
                    description={imageSpecifics.description}
                    favorites={imageSpecifics.star_count}
                    downloads={imageSpecifics.pull_count}
                    link={`https://hub.docker.com/r/${imageSpecifics.namespace}/${imageSpecifics.name}`}
                />
            );
        }

        this.setState({
            images: images
        });
    }

    render() {
        return (
            <IndexItem title={"Images"}>
                {this.state.images}
            </IndexItem>
        );
    }
}