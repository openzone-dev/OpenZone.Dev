import React, {Component} from "react";
import style from "./AboutUs.module.css";
import IndexItem from "./IndexItem";


export default class AboutUs extends Component {
    render() {
        return (
            <IndexItem title={"About Us"}>
                <p className={style.description}>
                    OpenZone(dot)Dev is a developer group focused on providing effective cross platform open source
                    solutions to all kinds of developers, in all kinds of fields. Even this page can be modified by
                    forking its repository, or can be modified to suit your own needs. All of our projects can be used in
                    commercial, and non-commercial scenarios alike.
                </p>
                <br />
                <p className={style.description}>
                    We support ARM64, ARMv7, x86 and other alternative architectures in our projects and containers.
                    These images are, most of the time, compiled and built on bare metal hardware. Don&apos;t see the
                    architecture you&apos;re looking for in one of our projects?, let us know!
                </p>
            </IndexItem>
        );
    }
}