import React, {Component} from "react";
import style from "./IndexItem.module.css"


export default class IndexItem extends Component {
    render() {
        return (
            <div className={style.item_root}>
                <div className={style.item_title}>
                    <p className={style.item_title_text}>{this.props.title.toUpperCase()}</p>
                </div>
                <div className={style.item_content}>
                    {this.props.children}
                </div>
            </div>
        );
    }
}