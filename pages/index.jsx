import Head from "next/head";
import AboutUs from "../components/index/AboutUs";
import GitlabRepo from "../components/index/GitlabRepo";
import DockerHub from "../components/index/DockerHub";


export default function Home() {
  return (
    <div className="body html">
      <div className="app_container overlay_parent ">
        <Head>
          <title>OpenZone.Dev</title>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          />

          <link rel="icon" href="/images/favicon.ico" />
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link
              href="https://fonts.googleapis.com/css2?family=Noto+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet"
          />
          <link
              rel="stylesheet"
              href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0"
          />
        </Head>
        <div className="app_background overlay_children"/>
        <div className="app_foreground overlay_children">
          <div className="app_content">
            <div className="app_content_containers">
              <div className="app_header">
                <p className="app_header_title">
                  OpenZone.Dev
                </p>
                <p className="app_header_description">
                  Open source technology group
                </p>
                <div className="app_header_links">
                  <a href="https://gitlab.com/openzone-dev"
                     target="_blank"
                     rel="noopener noreferrer"
                     className="header_link">
                    Gitlab
                  </a>
                  <a
                    href="https://hub.docker.com/u/openzonedev"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="header_link"
                  >
                    Docker Hub
                  </a>
                </div>
              </div>
              <div className="app_projects">
                <AboutUs />
                <GitlabRepo />
                <DockerHub />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
